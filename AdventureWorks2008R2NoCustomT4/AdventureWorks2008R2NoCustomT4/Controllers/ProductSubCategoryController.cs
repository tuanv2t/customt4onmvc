﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdventureWorks2008R2NoCustomT4.DAL;

namespace AdventureWorks2008R2NoCustomT4.Controllers
{
    public class ProductSubCategoryController : Controller
    {
        private AdventureWorks2008R2Entities db = new AdventureWorks2008R2Entities();

        // GET: /ProductSubCategory/
        public ActionResult Index()
        {
            var productsubcategories = db.ProductSubcategories.Include(p => p.ProductCategory);
            return View(productsubcategories.ToList());
        }

        // GET: /ProductSubCategory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSubcategory productsubcategory = db.ProductSubcategories.Find(id);
            if (productsubcategory == null)
            {
                return HttpNotFound();
            }
            return View(productsubcategory);
        }

        // GET: /ProductSubCategory/Create
        public ActionResult Create()
        {
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategories, "ProductCategoryID", "Name");
            return View();
        }

        // POST: /ProductSubCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ProductSubcategoryID,ProductCategoryID,Name,rowguid,ModifiedDate")] ProductSubcategory productsubcategory)
        {
            if (ModelState.IsValid)
            {
                db.ProductSubcategories.Add(productsubcategory);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductCategoryID = new SelectList(db.ProductCategories, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
            return View(productsubcategory);
        }

        // GET: /ProductSubCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSubcategory productsubcategory = db.ProductSubcategories.Find(id);
            if (productsubcategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategories, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
            return View(productsubcategory);
        }

        // POST: /ProductSubCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ProductSubcategoryID,ProductCategoryID,Name,rowguid,ModifiedDate")] ProductSubcategory productsubcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productsubcategory).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategories, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
            return View(productsubcategory);
        }

        // GET: /ProductSubCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSubcategory productsubcategory = db.ProductSubcategories.Find(id);
            if (productsubcategory == null)
            {
                return HttpNotFound();
            }
            return View(productsubcategory);
        }

        // POST: /ProductSubCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductSubcategory productsubcategory = db.ProductSubcategories.Find(id);
            db.ProductSubcategories.Remove(productsubcategory);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
