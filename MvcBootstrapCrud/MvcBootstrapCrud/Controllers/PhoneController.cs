﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcBootstrapCrud.DAL;
using MvcBootstrapCrud.Models;
using System.Linq.Dynamic;
using System.Threading.Tasks;
using System.Net;


namespace MvcBootstrapCrud.Controllers
{
    public class PhoneController : Controller
    {
        private MobilesEntities db = new MobilesEntities();


        // GET: /Phone/
        public ActionResult Index(string filter = null, int page = 1, int pageSize = 5, string sort = "PhoneId", string sortdir = "DESC")
        {
            if (db.Phone.Count() == 0)
            {
                var phones = new List<Phone>
                {
                new Phone{Model="Samsung Galaxy Note 1", Company="Samsung",Price= 339},
                new Phone{Model="Samsung Galaxy Note 2", Company="Samsung",Price= 399},
                new Phone{Model="Samsung Galaxy S III", Company="Samsung",Price= 217},
                new Phone{Model="Samsung Galaxy S IV", Company="Samsung",Price= 234},
                new Phone{Model="iPhone 5", Company="Apple",Price= 456}
                };

                phones.ForEach(p => db.Phone.Add(p));
                db.SaveChanges();
            }
            var records = new PagedList<Phone>();
            ViewBag.filter = filter;
            records.Content = db.Phone
                        .Where(x => filter == null ||
                                (x.Model.Contains(filter))
                                   || x.Company.Contains(filter)
                              )
                        .OrderBy(sort + " " + sortdir)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            // Count
            records.TotalRecords = db.Phone
                         .Where(x => filter == null ||
                               (x.Model.Contains(filter)) || x.Company.Contains(filter)).Count();

            records.CurrentPage = page;
            records.PageSize = pageSize;

            return View(records);

        }


        // GET: /Phone/Details/5
        public ActionResult Details(int id = 0)
        {
            Phone phone =  db.Phone.Find(id);
            if (phone == null)
            {
                return HttpNotFound();
            }
            return PartialView("Details", phone);
        }


        // GET: /Phone/Create
        [HttpGet]
        public ActionResult Create()
        {
            var phone = new Phone();
            return PartialView("Create", phone);
        }


        // POST: /Phone/Create
        [HttpPost]
        public JsonResult Create(Phone phone)
        {
            if (ModelState.IsValid)
            {
                db.Phone.Add(phone);
                db.SaveChanges();
                return Json(new { success = true });
            }
            return Json(phone, JsonRequestBehavior.AllowGet);
        }


        // GET: /Phone/Edit/5
        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            var phone = db.Phone.Find(id);
            if (phone == null)
            {
                return HttpNotFound();
            }

            return PartialView("Edit", phone);
        }


        // POST: /Phone/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Phone phone)
        {
            if (ModelState.IsValid)
            {
                db.Entry(phone).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { success = true });
            }
            return PartialView("Edit", phone);
        }

        //
        // GET: /Phone/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Phone phone = db.Phone.Find(id);
            if (phone == null)
            {
                return HttpNotFound();
            }
            return PartialView("Delete", phone);
        }


        //
        // POST: /Phone/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var phone = db.Phone.Find(id);
            db.Phone.Remove(phone);
            db.SaveChanges();
            return Json(new { success = true });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
