--drop table Phone
Create Table Phone(
PhoneId INT IDENTITY(1,1),
Model NVARCHAR(255),
Company NVARCHAR(255),
Price decimal,
PRIMARY KEY(PhoneId)
)