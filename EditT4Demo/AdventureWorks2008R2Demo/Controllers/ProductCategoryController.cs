﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdventureWorks2008R2Demo.DAL;
using AdventureWorks2008R2Demo.Models;
namespace AdventureWorks2008R2Demo.Controllers
{
    public class ProductCategoryController : Controller
    {
        private AdventureWorks2008R2Entities db = new AdventureWorks2008R2Entities();

        // GET: /ProductCategory/
        public ActionResult Index(string filter = null, int page = 1, int pageSize = 5, string sort = "ProductCategoryID", string sortdir = "DESC")
        {
            var records = new PagedList<ProductCategory>();
            ViewBag.filter = filter;
            records.Content = db.ProductCategory
                        .Where(x => filter == null 
																		|| x.Name.Contains(filter) 
									                              )
                        .OrderBy(sort + " " + sortdir)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            // Count
            records.TotalRecords = db.ProductCategory
                         .Where(x => filter == null
							   
							  										|| x.Name.Contains(filter) 
																   
							   ).Count();

            records.CurrentPage = page;
            records.PageSize = pageSize;

            return View(records);
        }

        // GET: /ProductCategory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCategory productcategory = db.ProductCategory.Find(id);
            if (productcategory == null)
            {
                return HttpNotFound();
            }
			return PartialView("Details", productcategory);
            //return View(productcategory);
        }

        // GET: /ProductCategory/Create
        public ActionResult Create()
        {
			var productcategory = new ProductCategory ();
			return PartialView("Create", productcategory);

        //    return View();
        }

        // POST: /ProductCategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ProductCategoryID,Name,rowguid,ModifiedDate")] ProductCategory productcategory)
        {
            if (ModelState.IsValid)
            {
                db.ProductCategory.Add(productcategory);
                db.SaveChanges();
                //return RedirectToAction("Index");
				return Json(new { success = true });
            }

            //return View(productcategory);
			 return Json(productcategory, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductCategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCategory productcategory = db.ProductCategory.Find(id);
            if (productcategory == null)
            {
                return HttpNotFound();
            }
              return PartialView("Edit", productcategory);
			//return View(productcategory);        
			}

        // POST: /ProductCategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ProductCategoryID,Name,rowguid,ModifiedDate")] ProductCategory productcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productcategory).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
				return Json(new { success = true });
            }
			return PartialView("Edit", productcategory);
            //return View(productcategory);
        }

        // GET: /ProductCategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductCategory productcategory = db.ProductCategory.Find(id);
            if (productcategory == null)
            {
                return HttpNotFound();
            }
			 return PartialView("Delete", productcategory);
            //return View(productcategory);
        }

        // POST: /ProductCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductCategory productcategory = db.ProductCategory.Find(id);
            db.ProductCategory.Remove(productcategory);
            db.SaveChanges();
            return Json(new { success = true });
			//return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
