﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdventureWorks2008R2Demo.DAL;
using AdventureWorks2008R2Demo.Models;
namespace AdventureWorks2008R2Demo.Controllers
{
    public class ProductSubcategoryController : Controller
    {
        private AdventureWorks2008R2Entities db = new AdventureWorks2008R2Entities();

        // GET: /ProductSubcategory/
        public ActionResult Index(string filter = null, int page = 1, int pageSize = 5, string sort = "ProductSubcategoryID", string sortdir = "DESC")
        {
            var productsubcategory = db.ProductSubcategory.Include(p => p.ProductCategory);
 var records = new PagedList<ProductSubcategory>();
            ViewBag.filter = filter;
            records.Content = db.ProductSubcategory
                        .Where(x => filter == null 
																		|| x.Name.Contains(filter) 
									                              )
                        .OrderBy(sort + " " + sortdir)
                        .Skip((page - 1) * pageSize)
                        .Take(pageSize)
                        .ToList();

            // Count
            records.TotalRecords = db.ProductSubcategory
                         .Where(x => filter == null
							   
							  										|| x.Name.Contains(filter) 
																   
							   ).Count();

            records.CurrentPage = page;
            records.PageSize = pageSize;

            return View(records);

             //return View(await productsubcategory.ToListAsync());
        }

        // GET: /ProductSubcategory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSubcategory productsubcategory = db.ProductSubcategory.Find(id);
            if (productsubcategory == null)
            {
                return HttpNotFound();
            }
			            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
			
			return PartialView("Details", productsubcategory);
            //return View(productsubcategory);
        }

        // GET: /ProductSubcategory/Create
        public ActionResult Create()
        {
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ProductCategoryID", "Name");
			var productsubcategory = new ProductSubcategory ();
			return PartialView("Create", productsubcategory);

        //    return View();
        }

        // POST: /ProductSubcategory/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ProductSubcategoryID,ProductCategoryID,Name,rowguid,ModifiedDate")] ProductSubcategory productsubcategory)
        {
            if (ModelState.IsValid)
            {
                db.ProductSubcategory.Add(productsubcategory);
                db.SaveChanges();
                //return RedirectToAction("Index");
				return Json(new { success = true });
            }

            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
            //return View(productsubcategory);
			 return Json(productsubcategory, JsonRequestBehavior.AllowGet);
        }

        // GET: /ProductSubcategory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSubcategory productsubcategory = db.ProductSubcategory.Find(id);
            if (productsubcategory == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
              return PartialView("Edit", productsubcategory);
			//return View(productsubcategory);        
			}

        // POST: /ProductSubcategory/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ProductSubcategoryID,ProductCategoryID,Name,rowguid,ModifiedDate")] ProductSubcategory productsubcategory)
        {
            if (ModelState.IsValid)
            {
                db.Entry(productsubcategory).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
				return Json(new { success = true });
            }
            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
			return PartialView("Edit", productsubcategory);
            //return View(productsubcategory);
        }

        // GET: /ProductSubcategory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSubcategory productsubcategory = db.ProductSubcategory.Find(id);
            if (productsubcategory == null)
            {
                return HttpNotFound();
            }
			            ViewBag.ProductCategoryID = new SelectList(db.ProductCategory, "ProductCategoryID", "Name", productsubcategory.ProductCategoryID);
			
			 return PartialView("Delete", productsubcategory);
            //return View(productsubcategory);
        }

        // POST: /ProductSubcategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductSubcategory productsubcategory = db.ProductSubcategory.Find(id);
            db.ProductSubcategory.Remove(productsubcategory);
            db.SaveChanges();
            return Json(new { success = true });
			//return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
